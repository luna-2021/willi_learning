"""
    Algoritmo Sumar2numeros
    var
        num1: float, num2: float, resultado: float
    fvar

    leer(num1)
    leer(num2)

    resultado = num1 + num2
    mostrar(resultado)

    Fin Algoritmo
"""

print('Algoritmo sumar 2 numeros')

num1: int
num2: int
result: float

num1 = int(input('Ingresa el numero 1: '))
num2 = int(input("Ingresa el numero 2: "))

result = num1 + num2

print('El resultado es: ', result)
